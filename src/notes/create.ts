import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { AWSError } from "aws-sdk";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import * as uuid from "uuid";
import { logger } from "../shared/logger";
import { dynamodb } from "./dynamodb";

export const create: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
    const timestamp: number = new Date().getTime();
    const data = JSON.parse(event.body);
    if (typeof data.text !== "string") {
        logger.error("Validation Failed");
        cb(new Error("Couldn't create the note."));
        return;
    }

    const params: DocumentClient.PutItemInput = {
        Item: {
            createdAt: timestamp,
            id: uuid.v1(),
            text: data.text,
            updatedAt: timestamp,
        },
        TableName: process.env.NOTES_TABLE,
    };

    dynamodb.put(params, (error: AWSError, _) => {
        // handle potential errors
        if (error) {
            logger.error(JSON.stringify(error));
            cb(new Error("Couldn't create the note item."));
            return;
        }

        // create a response
        const response: object = {
            body: JSON.stringify(params.Item),
            statusCode: 200,
        };

        logger.info(JSON.stringify(response));
        cb(null, response);
    });
};
