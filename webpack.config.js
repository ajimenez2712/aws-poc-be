const path = require("path");
const slsw = require("serverless-webpack");

const entries = {};

Object.keys(slsw.lib.entries).forEach(key => (
  entries[key] = ["./source-map-install.js", slsw.lib.entries[key]]
));

module.exports = {
  devtool: "source-map",
  entry: entries,
  module: {
    loaders: [
      { test: /\.ts(x?)$/, loader: "ts-loader" },
    ],
  },
  output: {
    filename: "[name].js",
    libraryTarget: "commonjs",
    path: path.join(__dirname, ".webpack"),
  },
  resolve: {
    extensions: [
      ".js",
      ".jsx",
      ".json",
      ".ts",
      ".tsx",
    ],
  },
  target: "node",
};
