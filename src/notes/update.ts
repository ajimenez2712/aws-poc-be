import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { AWSError } from "aws-sdk";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { logger } from "../shared/logger";
import { dynamodb } from "./dynamodb";

export const update: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
    const timestamp: number = new Date().getTime();
    const data = JSON.parse(event.body);

    // validation
    if (typeof data.text !== "string") {
        logger.error("Validation failed");
        cb(null, {
            body: "Couldn't update the note item.",
            headers: { "Content-Type": "text/plain" },
            statusCode: 400,
        });
        return;
    }

    const params: DocumentClient.UpdateItemInput = {
        ExpressionAttributeNames: {
            "#note_text": "text",
        },
        ExpressionAttributeValues: {
            ":text": data.text,
            ":updatedAt": timestamp,
        },
        Key: {
            id: event.pathParameters.id,
        },
        ReturnValues: "ALL_NEW",
        TableName: process.env.NOTES_TABLE,
        UpdateExpression: "SET #note_text = :text, updatedAt = :updatedAt",
    };

    dynamodb.update(params, (error: AWSError, result: DocumentClient.UpdateItemOutput) => {
        // handle potential errors
        if (error) {
            logger.error(JSON.stringify(error));
            cb(null, {
                body: "Couldn't update the note item",
                headers: { "Content-Type": "plain/text" },
                statusCode: error.statusCode || 501,
            });
            return;
        }

        // create a response
        const response: object = {
            body: JSON.stringify(result.Attributes),
            statusCode: 200,
        };
        logger.info(JSON.stringify(response));
        cb(null, response);
    });
};
