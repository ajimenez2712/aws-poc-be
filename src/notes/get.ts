import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { AWSError } from "aws-sdk";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { logger } from "../shared/logger";
import { dynamodb } from "./dynamodb";

export const get: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
    const params: DocumentClient.GetItemInput = {
        Key: {
            id: event.pathParameters.id,
        },
        TableName: process.env.NOTES_TABLE,
    };

    // fetch note from the database
    dynamodb.get(params, (error: AWSError, result: DocumentClient.GetItemOutput) => {
        // handle potential errors
        if (error) {
            logger.error(JSON.stringify(error));
            cb(null, {
                body: "Couldn't fetch the note item.",
                headers: { "Content-Type": "text/plain" },
                statusCode: error.statusCode || 501,
            });
            return;
        }

        // create a response
        const response: object = {
            body: JSON.stringify(result.Item),
            statusCode: 200,
        };
        logger.info(JSON.stringify(response));
        cb(null, response);
    });
};
