import { expect } from "chai";
import { hello } from "./hello-world";

describe("hello function", () => {

    it("should return hello world", () => {
        const result = hello();
        expect(result).to.equal("Hello world!");
    });
});
