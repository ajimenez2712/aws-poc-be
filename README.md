# Install dependencies
```shell
npm install
```

# Start local DynamoDB instance and offline serverless simulator
```shell
serverless offline start
```

# Create 3 notes
```shell
curl -X POST -H "Content-Type:application/json" http://localhost:3000/notes --data '{ "text": "Learn Serverless 1" }'
curl -X POST -H "Content-Type:application/json" http://localhost:3000/notes --data '{ "text": "Learn Serverless 2" }'
curl -X POST -H "Content-Type:application/json" http://localhost:3000/notes --data '{ "text": "Learn Serverless 3" }'
```

# List all notes
```shell
curl http://localhost:3000/notes
```

# Update a note
Note: Replace $NOTE_ID with the desired ID, you can get it from the Create response or with a List

```shell
curl -X PUT -H "Content-Type:application/json" http://localhost:3000/notes/$NOTE_ID --data '{ "text": "Learn Serverless :)" }'
```

# Get a note
Note: Replace $NOTE_ID with the desired ID, you can get it from the Create response or with a List

```shell
curl http://localhost:3000/notes/$NOTE_ID
```

# Delete a note
Note: Replace $NOTE_ID with the desired ID, you can get it from the Create response or with a List

```shell
curl -X DELETE http://localhost:3000/notes/$NOTE_ID
```
