import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { AWSError } from "aws-sdk";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { logger } from "../shared/logger";
import { dynamodb } from "./dynamodb";

export const remove: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
    const params: DocumentClient.DeleteItemInput = {
        Key: {
            id: event.pathParameters.id,
        },
        TableName: process.env.NOTES_TABLE,
    };

    // delete the note from the database
    dynamodb.delete(params, (error: AWSError, _) => {
        // handle potential errors
        if (error) {
            logger.error(JSON.stringify(error));
            cb(null, {
                body: "Couldn't remove the note item",
                headers: { "Content-Type": "plain/text" },
                statusCode: error.statusCode || 501,
            });
            return;
        }

        const response: object = {
            body: JSON.stringify({}),
            statusCode: 200,
        };
        logger.info(JSON.stringify(response));
        cb(null, response);
    });
};
