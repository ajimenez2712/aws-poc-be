import { APIGatewayEvent, Callback, Context, Handler } from "aws-lambda";
import { AWSError } from "aws-sdk";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { logger } from "../shared/logger";
import { dynamodb } from "./dynamodb";

export const list: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
    const params: DocumentClient.ScanInput = {
        TableName: process.env.NOTES_TABLE,
    };

    dynamodb.scan(params, (error: AWSError, result: DocumentClient.ScanOutput) => {
        // handle potential errors
        if (error) {
            logger.error(JSON.stringify(error));
            cb(null, {
                body: "Couldn't fetch the notes items.",
                headers: { "Content-Type": "text/plain" },
                statusCode: error.statusCode || 501,
            });
            return;
        }

        // create a reponse
        const response: object = {
            body: JSON.stringify(result.Items),
            statusCode: 200,
        };
        logger.info(JSON.stringify(response));
        cb(null, response);
    });
};
